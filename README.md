**MetroPay**

Visio:
MetroPay on pankissa asioimiseen tarkoitettu työpöytäsovellus. Sovelluksella hallitaan käyttäjän pankkitiliä ja siihen liittyviä rahallisia tapahtumia. MetroPay poikkeaa muista nykyaikaisista pankkien käyttöliittymistä siten, että se ei toimi verkkosovelluksena, vaan työpöytäsovelluksena poistaen verkkoselaimen prosessista tehden asioimisesta yksinkertaisempaa. Sovelluksen ominaisuudet ovat myös pelkistettyjä käyttökokemuksen helpottamiseksi. 

Kehitysympäristö ja kirjastot:
Kyseessä on Maven -projekti. Projekti on koodattu pääasiassa Java-kielellä ja ulkoasun määrittelyssä on hyödynnetty myös css -tyylikieltä. Projektissa on käytetty seuraavia Java -kirjastoja: JavaFX, MariaDB, JDBC, Hibernate ja JUnit. Maven koonnissa käytetään Maven-kääntäjää, Surefiren, JavaFX:n ja JaCoCo:n Maveniin tarkoitettuja lisäosia.

Sovellus käyttää ulkoista rajapintaa valuuttakurssien päivittämiseen. Käytetty rajapinta:
https://github.com/fawazahmed0/currency-api

Asennus- ja konfigurointiohjeet:
Varsinaisen sovelluksen käyttöönotto ei vaadi erityisempiä toimenpiteitä, toimiva Java-kääntäjä riittää sovelluksen ajamiseen. Sovellus on Maven -projekti, eli kaikki tarvittavat kirjastot ja lisäosat on määritelty pom.xml -tiedostossa ja kääntäjä osaa hakea ne ilman että käyttäjän tarvitsee niitä manuaalisesti lisätä.

Sovellukseen liittyy SQL-tietokanta, joka toimii Metropolian verkossa olevalla educloud -palvelimella. Lähtökohtaisesti tietokannan käyttö vaatii siis yhdistämistä Metropolian verkkoon esimerkiksi VPN-yhteydellä. Tätä tietokantaa käytettäessä ei muita toimenpiteitä vaadita.

Jos taas käyttäjällä ei ole pääsyä olemassa olevaan tietokantaan, niin hän voi luoda oman tietokannan paikallisesti tai haluamalleen palvelimelle. Sovelluksen yhteydessä olevasta Documents-kansiosta löytyy metropay_database.sql-tiedosto, josta löytyy tiedot joilla alkuperäinen tietokanta on luotu ja jolla käyttäjä voi luoda uuden vastaavan toimivan tietokannan. Näin toimiessa täytyy myös hibernate.cfg.xml -tiedosto muokata yhteensopivaksi uuteen tietokantaan, tämä tiedosto löytyy src/main/resources-hakemistopolusta.

Käyttöohje:
Sovelluksen käyttöohje löytyy Documents-kansiosta.


[OTP2-projekti, ryhmä 7]
Mathias Karhu, Otso Poussa, Joni Jääskeläinen ja Tatu Talvikko
